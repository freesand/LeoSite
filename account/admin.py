from django.contrib import admin
from .models import UserProfile, UserInfo


# Register your models here.
class UserProfileAdmin(admin.ModelAdmin):
    list_display = ("user", "birth", "phone")  # 列表显示项
    list_filter = ("phone",)  # 过滤器


class UserInfoAdmin(admin.ModelAdmin):
    list_display = ("user", "school", "company", "profession", "address", "aboutme", "photo")
    list_filter = ("school", "company", "profession")


admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(UserInfo, UserInfoAdmin)
