"""views.py 视图
"""
from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required  # 登录检验装饰器函数
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect  # 页面自动跳转
from .forms import LoginForm, RegistrationForm, UserProfileForm, UserInfoForm, UserForm
from .models import UserProfile, UserInfo


# Create your views here.
def user_login(request):
    """登录视图函数
    """
    if request.method == 'POST':
        login_form = LoginForm(request.POST)
        if login_form.is_valid():
            cd = login_form.cleaned_data
            user = authenticate(username=cd['username'],
                                password=cd['password'])
            if user:
                login(request, user)
                # return HttpResponse("欢迎登录")
                return HttpResponseRedirect("/")
            else:
                return HttpResponse("错误的账号或密码")
        else:
            return HttpResponse("输入不正确")
    if request.method == 'GET':
        login_form = LoginForm()
        return render(request, 'account/login.html', {'form': login_form})


def register(request):
    """注册视图函数
    """
    if request.method == "POST":
        user_form = RegistrationForm(request.POST)
        userprofile_form = UserProfileForm(request.POST)  # 额外注册信息表单类
        if user_form.is_valid():
            new_user = user_form.save(commit=False)  # 仅生成数据对象
            new_user.set_password(user_form.cleaned_data["password"])
            new_user.save()  # 设置密码后再提交写入数据库
            # 额外注册信息
            new_profile = userprofile_form.save(commit=False)
            new_profile.user = new_user
            new_profile.save()
            UserInfo.objects.create(user=new_user)  # 在account_userinfo表添加用户ID
            # return HttpResponse("注册成功！")
            return HttpResponseRedirect(reverse("account:user_login"))
        else:
            return HttpResponse("对不起，注册失败。")
    else:
        user_form = RegistrationForm()
        userprofile_form = UserProfileForm()  # 额外注册信息表单
        return render(request, "account/register.html",
                      {"form": user_form, "profile": userprofile_form})


@login_required(login_url="/account/login/")  # 指定未登录用户跳转目标
def myself(request):
    """显示个人信息
    """
    user = User.objects.get(username=request.user.username)
    userprofile = UserProfile.objects.get(user=user)
    userinfo = UserInfo.objects.get(user=user)
    return render(request, "account/myself.html", {"user": user, "userinfo": userinfo, "userprofile": userprofile})


@login_required(login_url="/account/login/")
def myself_edit(request):
    """编辑个人信息
    """
    user = User.objects.get(username=request.user.username)
    userprofile = UserProfile.objects.get(user=user)
    userinfo = UserInfo.objects.get(user=user)
    if request.method == "POST":
        user_form = UserForm(request.POST)
        userprofile_form = UserProfileForm(request.POST)
        userinfo_form = UserInfoForm(request.POST)
        if user_form.is_valid() * userprofile_form.is_valid() * userinfo_form.is_valid():
            user_cd = user_form.cleaned_data
            userprofile_cd = userprofile_form.cleaned_data
            userinfo_cd = userinfo_form.cleaned_data
            # print(user_cd["email"])
            user.email = user_cd["email"]
            userprofile.birth = userprofile_cd["birth"]
            userprofile.phone = userprofile_cd["phone"]
            userinfo.school = userinfo_cd["school"]
            userinfo.company = userinfo_cd["company"]
            userinfo.profession = userinfo_cd["profession"]
            userinfo.address = userinfo_cd["address"]
            userinfo.aboutme = userinfo_cd["aboutme"]
            user.save()
            userprofile.save()
            userinfo.save()
        return HttpResponseRedirect("/account/my-information")
    else:
        user_form = UserForm(instance=request.user)
        userprofile_form = UserProfileForm(initial={"birth": userprofile.birth, "phone": userprofile.phone})
        userinfo_form = UserInfoForm(initial={
            "school": userinfo.school, "company": userinfo.company, "profession": userinfo.profession,
            "address": userinfo.address, "aboutme": userinfo.aboutme
        })
        return render(request, "account/myself_edit.html", {
            "user_form": user_form, "userprofile_form": userprofile_form, "userinfo_form": userinfo_form
        })


@login_required(login_url="/account/login/")
def my_image(request):
    """头像图片
    """
    if request.method == "POST":
        img = request.POST["img"]
        userinfo = UserInfo.objects.get(user=request.user.id)
        userinfo.photo = img
        userinfo.save()
        return HttpResponse("1")
    else:
        return render(request, "account/imgcrop.html",)
