"""forms.py 表单
"""
from django import forms
from django.contrib.auth.models import User  # 使用Django默认用户模型
from .models import UserProfile, UserInfo  # 引入新用户模型，个人信息模型


class LoginForm(forms.Form):
    """登录表单类
    """
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)


class RegistrationForm(forms.ModelForm):  # 需要写数据库则继承ModelForm
    """注册表单类
    """
    # 定义新属性覆盖User模型中的字段
    password = forms.CharField(label="密码", widget=forms.PasswordInput)
    password2 = forms.CharField(label="确认密码", widget=forms.PasswordInput)

    class Meta:
        """内部类，表单类应用的数据模型
        """
        model = User
        fields = ("username", "email")  # 写入字段，也可用exclude排除字段

    def clean_password2(self):
        """检验输入的密码
        password2.is_valid()时执行
        """
        cd = self.cleaned_data
        if cd["password"] != cd["password2"]:
            raise forms.ValidationError("两次输入的密码不匹配")
        return cd["password2"]


class UserProfileForm(forms.ModelForm):
    """额外注册信息表单类
    """
    class Meta:
        model = UserProfile
        fields = ("phone", "birth")


class UserInfoForm(forms.ModelForm):
    """用户信息表单类
    """
    class Meta:
        model = UserInfo
        fields = ("school", "company", "profession", "address", "aboutme", "photo")


class UserForm(forms.ModelForm):
    """用户表单类，对应auth_user表
    """
    class Meta:
        model = User
        fields = ("email",)  
