from django.apps import AppConfig


class AccountConfig(AppConfig):
    name = 'account'
    verbose_name = "账号"
    verbose_name_plural = verbose_name
