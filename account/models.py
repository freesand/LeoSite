from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class UserProfile(models.Model):  # 将建立account_userprofile表
    """增加自定义用户属性
    """
    # UserProfile与User一对一，Django2要求有on_delete参数
    user = models.OneToOneField(User, unique=True, on_delete=models.CASCADE)
    birth = models.DateField(blank=True, null=True, verbose_name="生日")
    phone = models.CharField(max_length=20, null=True, verbose_name="电话")

    class Meta:
        verbose_name = "用户信息"
        verbose_name_plural = verbose_name

    def __str__(self):
        return "user {}".format(self.user.username)


class UserInfo(models.Model):
    """个人信息模型类
    """
    user = models.OneToOneField(User, unique=True, on_delete=models.CASCADE)
    school = models.CharField(max_length=100, blank=True)
    company = models.CharField(max_length=100, blank=True)
    profession = models.CharField(max_length=100, blank=True)
    address = models.CharField(max_length=100, blank=True)
    aboutme = models.TextField(blank=True)
    photo = models.ImageField(blank=True)

    class Meta:
        verbose_name = "用户详情"
        verbose_name_plural = verbose_name

    def __str__(self):
        return "user {}".format(self.user.username)
