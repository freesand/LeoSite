from django.apps import AppConfig


class ArticleConfig(AppConfig):
    name = 'article'
    verbose_name = "文章"
    verbose_name_plural = verbose_name
