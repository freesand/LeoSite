from django.urls import path, re_path
from django.conf.urls import url
from . import views, list_views

urlpatterns = [
    path('article-column/', views.article_column, name='article_column'),
    path('rename-column/', views.rename_article_column, name='rename_article_column'),
    path('del-column/', views.del_article_column, name='del_article_column'),
    path('article-post/', views.article_post, name="article_post"),
    path('article-list/', views.article_list, name="article_list"),
    # path('article-detail/<int:id>/<slug:slug>/', views.article_detail, name="article_detail"),
    re_path('article-detail/(?P<id>\d+)/(?P<slug>[-\w]+)/$', views.article_detail, name="article_detail"),  # 上两写法等价
    path('del-article/', views.del_article, name="del_article"),
    path('edit-article/<int:id>/<slug:slug>/', views.edit_article, name="edit_article"),
    path('article-titles/', list_views.article_titles, name="article_titles"),
    path('list-article-detail/<int:id>/<slug:slug>/', list_views.list_article_detail, name="list_article_detail"),
    path('article-titles/<username>', list_views.article_titles, name="author_articles"),
    path('like-article/', list_views.like_article, name="like_article"),
]